import java.util.Scanner;

public class Array10 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int elemnetNum = 0, intCheck = 0;
        System.out.print("Please input number of elemnet: ");
        elemnetNum = sc.nextInt();
        int arr[] = new int[elemnetNum];
        for (int i = 0; i < arr.length; i++) {
            System.out.print("Element " + i + ": ");
            arr[i] = sc.nextInt();
        }
        System.out.print("All number: ");
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length; j++) {
                if (j == i) {
                    break;
                }
                if (arr[j] == arr[i]) {
                    intCheck += 1;
                    continue;
                }

            }
            if (intCheck == 0) {
                System.out.print(arr[i] + " ");
            }
            intCheck = 0;
        }
        sc.close();
    }
}
