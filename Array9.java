import java.util.Scanner;

public class Array9 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int arr[] = new int[3];
        int i = 0, numToCheck = 0;
        while (i < arr.length) {
            System.out.print("Please input arr[" + i + "]: ");
            arr[i] = sc.nextInt();
            i++;
        }
        i = 0;
        System.out.print("arr = ");
        while (i < arr.length) {
            System.out.print(arr[i] + " ");
            i++;
        }
        System.out.println();
        System.out.print("please input search value: ");
        numToCheck = sc.nextInt();
        int j = 0;
        for (i = 0; i < arr.length; i++) {
            if (arr[i] == numToCheck) {
                j = 1;
                break;
            }
        }
        if (j == 1) {
            System.out.println("found at index: " + i);
        } else {
            System.out.println("not found");
        }
        sc.close();
    }
}
