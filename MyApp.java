import java.util.Scanner;

public class MyApp {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int choice = 0;
        sayHello();
        while (true) {
            choice = sayMenu();
            if (choice == 1) {
                printHelloNTimes();
            }
            if (choice == 2) {
                add2Number();
            }
            if (choice == 3) {
                break;
            }
        }
        System.out.println("\nBye!!");
    }

    private static void add2Number() {
        Scanner sc=new Scanner(System.in);
        int num1=0,num2=0,result=0;
        System.out.print("\nPlease input first number: ");
        num1 = sc.nextInt();
        System.out.print("\nPlease input second number: ");
        num2 = sc.nextInt();
        result = num1+num2;
        System.out.println("\nResult = "+result);
        System.out.println();
    }

    private static void printHelloNTimes() {
        Scanner sc = new Scanner(System.in);
        int time = 0;
        System.out.print("\nPlease input time: ");
        time = sc.nextInt();
        for (int i = 0; i < time; i++) {
            System.out.println("\nHello World!!!");
        }
        System.out.println();
    }

    static int sayMenu() {
        Scanner sc = new Scanner(System.in);
        int choice = 0;
        while (true) {
            System.out.println("---Menu---\n");
            System.out.println("1.Print Hello World N Times\n");
            System.out.println("2.Add 2 Number\n");
            System.out.println("3.Exit\n");
            System.out.print("Please input your choice(1-3): ");
            choice = sc.nextInt();
            if (choice >= 1 & choice <= 3) {
                break;
            }
            System.out.println("Error: Please input between 1-3");
        }
        return choice;

    }

    private static void sayHello() {
        System.out.println("Welcome to my app!!!\n");
    }

}
