import java.util.Scanner;

public class Array8 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int arr[] = new int[3];
        int sum = 0, min = 0, max = 0;
        for (int i = 0; i < arr.length; i++) {
            System.out.print("Please input arr[" + i + "]: ");
            arr[i] = sc.nextInt();
            sum += arr[i];
        }
        System.out.print("arr = ");
        min = arr[0];
        max = arr[0];
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
            if (arr[i] <= min) {
                min = arr[i];
            }
            if (arr[i] >= max) {
                max = arr[i];
            }
        }
        System.out.println();
        System.out.println("sum = " + sum);
        System.out.println("avg = " + (float) (sum) / arr.length);
        System.out.println("min = " + min);
        System.out.println("max = " + max);
        sc.close();
    }
}
