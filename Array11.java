import java.util.Scanner;

public class Array11 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int add1=0,add2=0,locker=0;
        int arr[] = new int[5];
        for(int i=0;i<arr.length;i++){
            arr[i] = (int) (Math.random()*100);
        }
        System.out.println("Array");
        for(int i=0;i<arr.length;i++){
            System.out.print(arr[i]+" ");
        }
        System.out.println();
        while(true){
            System.out.print("Please input index: ");
            add1= sc.nextInt();
            add2= sc.nextInt();
            locker = arr[add1];
            arr[add1] = arr[add2];
            arr[add2] = locker;
            locker=0;

            for(int i=0;i<arr.length;i++){
                System.out.print(arr[i]+" ");
            }
            System.out.println();

            if(arr[0]>arr[1]){
                continue;
            }
            if(arr[1]>arr[2]){
                continue;
            }
            if(arr[2]>arr[3]){
                continue;
            }
            if(arr[3]>arr[4]){
                continue;
            }
            else{
                break;
            }

        }
        System.out.println("You win!!!");
        sc.close();
    }
}
